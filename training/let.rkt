#lang scheme

(define soma1 (lambda (x) (+ x 1)))
(define resultado-let
  (let ((x (soma1 5))
        (y (* 4 5))
        (z 10))
      (+ x (- y z))))

(define resultado-let-lambda
  ((lambda (x y z) (+ x (- y z))) (soma1 5) (* 4 5) 10))

(displayln "Resultados let: ")
resultado-let ; 16
resultado-let-lambda ; 16

(define resultado-let*
  (let* ((x (soma1 5))
         (y (* 4 x))
         (z (+ x y)))
    (+ x (- y z))))

(define resultado-let*-lambda
  ((lambda (x) ((lambda (y) ((lambda (z) (+ x (- y z))) (+ x y))) (* 4 x)) ) (soma1 5)))

(displayln "Resultados let*: ")
resultado-let* ; 0
resultado-let*-lambda ; 0

