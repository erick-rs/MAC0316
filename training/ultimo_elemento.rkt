#lang racket

(define (ultimo lista)
  (if (null? (cdr lista))
    (car lista)
    (ultimo (cdr lista))))

(display "Último elemento de '(1 2 3 4 5): ")
(ultimo '(1 2 3 4 5))
